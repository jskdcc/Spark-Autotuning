/**
 * 
 */
package graybox;
/**
*@author tonychao 
*功能
*/

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.json.JSONException;

import fetcher.SparkConfiguration;
import others.Global;
import shellInterface.UserSubmitInterface_test;

/**
 * @author asus
 *
 */
public class GrayBoxConf {
	String app_md5,className;
	public  GrayBoxConf(String app_md5,String className) {
		this.app_md5=app_md5;
		this.className=className;
	}
	
	//RRS搜索
	public SparkConfiguration searchRRS() throws IOException, JSONException, InterruptedException {
		SparkConfiguration configuration=null;
		ParameterSearch tmParameterSearch= new ParameterSearch();
		if(Global.USE_DB_Flag==false){ //使用数据转储文件
			tmParameterSearch.init(this.app_md5, this.className,Global.PYTHON_DAEMON_PATH_FROM_FILE);
			configuration=tmParameterSearch.searchRRS(new ArrayList<String>(Arrays.asList("group0","group1"))); //区别只在于python文件而已
			UserSubmitInterface_test.UIOutPut("搜索:"+String.valueOf(tmParameterSearch.callCount));
			tmParameterSearch.end();
		}
		else {//使用数据库
			tmParameterSearch.init(this.app_md5, this.className,Global.PYTHON_DAEMON_PATH_FROM_DB);
			configuration=tmParameterSearch.searchRRS(new ArrayList<String>(Arrays.asList("group0","group1")));
			tmParameterSearch.end();
		}


		return configuration;
	
		
		/**
		*UNDONE
		// TODO Auto-generated method stub
		ArrayList<String> tmp = new ArrayList<String>();
		//指定参数空间
		ParameterSearch tmpModel =new ParameterSearch();
		tmpModel.init(app_md5,className);
		//搜索
		TurningPara para = tmpModel.searchRRS();
		tmpModel.end();
		System.out.println("调用预测器次数\t"+tmpModel.callCount);
		System.out.println("完成exploit 次数\t"+tmpModel.exploitCount);
		return conf;
	}
	**/
		
		
	}
	
	
	
	public SparkConfiguration searchGradientDescent() {
		SparkConfiguration configuration= null;
		
		
		return configuration;
	}
	
	
}
